﻿using UnityEngine;
using System.Collections.Generic;
using System;

/// <summary>
/// the test script to let the train always point toward the next waypoint 
/// Written 2017 by Aaron Schweighöfer
/// </summary>
public class Train_LookToWayPoint : MonoBehaviour
{
    public float rotationSpeed = 6f;
    public float distanceToWayPoint = 2;
    [SerializeField]
    WayPointScript currentTarget = null;
    [SerializeField]
    WayPointScript oldTarget;
    Train_MoveForward thisTrainPart;
    Train_CarValueStorrage thisCarValue;
    Vector3 currentTargetPosition = Vector3.zero;
    Transform carAhead;
    Transform leadTrain;
    bool isleadtrain = false;
	public bool atStation = false;

    // Use this for initialization
    void Start ()
    {
        thisCarValue = GetComponent<Train_CarValueStorrage>();
        currentTarget = thisCarValue.GetCurrentTargetWayPoint;
        carAhead = thisCarValue.GetCarAhead;
        isleadtrain = thisCarValue.GetisLeadtrain;
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        if (currentTarget == null)
        {
            currentTarget = thisCarValue.GetCurrentTargetWayPoint;
            return;
        }
        currentTargetPosition = currentTarget.transform.position;
        if (isleadtrain==true)
        {
            if (currentTarget == null)
            {
                return;
            }
            TrainCheckTarget();
        }
        else
        {
            CarCheckTarget();
        }
        LookToWayPoint();
    }

    private void LookToWayPoint()
    {
        Vector3 lookAtDirection = (currentTargetPosition - transform.position).normalized;
        Quaternion rotateToTarget = Quaternion.LookRotation(lookAtDirection);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotateToTarget, Time.fixedDeltaTime * rotationSpeed);
    }

    void TrainCheckTarget()
    {

        if (Vector3.Distance(currentTargetPosition, transform.position) < distanceToWayPoint)
        {
            //disable the old indication if there is any.
            if (currentTarget.isRailSwitch == true)
            {
                currentTarget.GetComponent<RailSwitch>().DisableIndication();
            }
            SetCurrentWaypoint();
            if (currentTarget == null)
            {
                return;
            }
            //enables the new indication of the next target if there is any
            if (currentTarget.isRailSwitch == true)
            {
                currentTarget.GetComponent<RailSwitch>().IndicateTrack(thisCarValue);
            }
            currentTargetPosition = currentTarget.transform.position;

			if (currentTarget.isTrainStation == true) {
				atStation = true;
				FindObjectOfType<StationTimer> ().reset = true;
			}
            else
            {
                atStation = false;
            }
        }
    }

    void CarCheckTarget()
    {
        currentTargetPosition = carAhead.transform.position;
    }

    void SetCurrentWaypoint()
    {
        WayPointScript neighbour1 = currentTarget.neighbour1;
        if (neighbour1 != oldTarget)
        {
            oldTarget = currentTarget;
            currentTarget = currentTarget.neighbour1;
        }
        else
        {
            oldTarget = currentTarget;
            currentTarget = currentTarget.neighbour2;
        }
        thisCarValue.SetCurrentTargetWayPoint = currentTarget;
        thisCarValue.SetOldTargetWaypoint = oldTarget;
    }
}
