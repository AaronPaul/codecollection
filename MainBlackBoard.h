
/*
/// Written By Aaron Schweighöfer 2018

This is the heartpiece of the Black Board and did get quite big because a short time frame and trouble to seperate it into two differnt files.
It is how i write non experimantal code and even through it is c++ shows quite well my progress in recent years The first part is just the iniliasation of the play work field
After that comes the basic functions of each of the buttons as well as regulating what to do when a login was succesfull. Usuall those part would be moved into serpated cpp files
but visual c++ refused to let me do that at that time so i just tried to sort the menu in Regions as good as i could. 
*/


#pragma once
//#include "Login.h"
#include "include.h"
#include "BlackBoardApi.h"
#include "FileExplorer.h"

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Collections::Generic;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

public ref class MainBlackBoard : public System::Windows::Forms::Form {
private:
	//the blackBoardName gets decided when the BlackBoard is created and should be uniquie. 
	//The creation of Blackboards does go byond the scope of this Mokup however so the Name will be set in the Konstructor
	String^ blackBoardName;
	String^ displayName;
	bool loggedIn;
	bool drawOpend;
	bool writeOpend;
	bool pictureOpend;
	bool chatOpend;
	bool mouseClick;
	int penSize;
	int chatTextSize;
	int writeTextSize;
	int moveOffsetX;
	int moveOffsetY;
	int sizeCheck;
	Color penColor;
	Color nameColor;
	Color writeColor;
	Login1^ loginForm;
	BlackBoardApi^ blackBoardApiClass;
    FileExplorernamespace::FileExplorerClass^ fileExplorer;
	RichTextBox^ currentSelectedTextBox;

	List <Point>^ drawpointList;
	List <Color>^ drawColorSteeps;
	List <int>^ drawSizeSteeps;
	List <List<Point>^>^ drawPointsSteeps;
	List <RichTextBox^>^ writeRichTextBoxList;
	List <PictureBox^>^ pictureList;

	System::ComponentModel::Container ^components;
	int buttonWidth;
	int buttonHeight;
	Image^ selectedImage;
public:
	MainBlackBoard(Void)
	{
		blackBoardName = "BlackBoard1";
		buttonWidth = 56;
		buttonHeight = 43;
		chatTextSize = 14;
		writeTextSize = 14;
		loginForm = gcnew Login1(this);
		blackBoardApiClass = gcnew BlackBoardApi();
		fileExplorer = gcnew  FileExplorernamespace::FileExplorerClass();
		loggedIn = false;
		drawOpend = false;
		writeOpend = false;
		pictureOpend = false;
		chatOpend = false;
		mouseClick = false;

		InitializeComponent();
		drawpointList = gcnew List<Point>();
		drawColorSteeps = gcnew List<Color>();
		drawSizeSteeps = gcnew List<int>();
		drawPointsSteeps = gcnew List<List<Point>^ >();
		writeRichTextBoxList = gcnew List<RichTextBox^>();
		pictureList = gcnew List<PictureBox^>();

		for (int i = 1; i < 20; i++)
		{
			draw_PenSizeComboBox->Items->Add((i).ToString());
		}
		for (int i = 10; i < 30; i++)
		{
			chat_TextSizeComboBox->Items->Add((i).ToString());
		}
		for (int i = 10; i < 30; i++)
		{
			write_TextSizeComboBox->Items->Add((i).ToString());
		}
		penColor = Color::Black;
		nameColor = Color::Black;
		writeColor = Color::Black;
		penSize = 1;
		draw_PenSizeComboBox->Text = penSize.ToString();
		////////FOR TESTING//////
		setBlackBoardToLoggedIn("test");
	}
protected:
	~MainBlackBoard()
	{
		if (components)
		{
			delete components;
		}
	}

#pragma region FormsAndWindows
private: System::Windows::Forms::Panel^  blackBoardPanel;
private: System::Windows::Forms::GroupBox^  buttonsGroupBox;
private: System::Windows::Forms::GroupBox^  drawButtonGroupBox;
private: System::Windows::Forms::GroupBox^  writeGroupBox;
private: System::Windows::Forms::GroupBox^  pictureGroupBox;
private: System::Windows::Forms::GroupBox^  chatButtonGroupBox;
//MainButtons:
private: System::Windows::Forms::Button^  cmd_UpdateBlackboard;
private: System::Windows::Forms::Button^  cmd_Chat;
private: System::Windows::Forms::Button^  cmd_leaveChat;
private: System::Windows::Forms::Button^  cmd_Picture;
private: System::Windows::Forms::Button^  cmd_LoginLogOut;
private: System::Windows::Forms::Button^  cmd_Draw;
private: System::Windows::Forms::Button^  cmd_Write;
private: System::Windows::Forms::Button^  cmd_Settings;
private: System::Windows::Forms::Button^  cmd_Map;
//DrawGroupForms:
private: System::Windows::Forms::Button^  cmd_leaveDraw;
private: System::Windows::Forms::Button^  cmd_Undo;
private: System::Windows::Forms::Button^  cmd_selectDrawColor;
private: System::Windows::Forms::ComboBox^ draw_PenSizeComboBox;
//WriteGroupForms:
private: System::Windows::Forms::Button^  cmd_leaveWrite;
private: System::Windows::Forms::Button^  cmd_AddNewTextField;
private: System::Windows::Forms::Button^  cmd_SelectWriteColor;
private: System::Windows::Forms::ComboBox^ write_TextSizeComboBox;
//PictureGroupForms:
private: System::Windows::Forms::Button^  cmd_leavePicture;
private: System::Windows::Forms::Button^  cmd_AddPicture;
		 
//ChatGroupForms:
private: System::Windows::Forms::Button^  cmd_TextChatColorDialog;
private: System::Windows::Forms::Button^  cmd_NameChatColorDialog;
private: System::Windows::Forms::TextBox^  chat_TextBox;
private: System::Windows::Forms::ComboBox^ chat_TextSizeComboBox;
private: System::Windows::Forms::RichTextBox^ chat_RichTextBox;

private: void InitializeComponent(void)
	{
	//Initilising everything:
		//Pannels and grouboxes First:
		this->blackBoardPanel = gcnew System::Windows::Forms::Panel();
		this->buttonsGroupBox = gcnew System::Windows::Forms::GroupBox();
		this->drawButtonGroupBox = gcnew System::Windows::Forms::GroupBox();
		this->writeGroupBox = gcnew System::Windows::Forms::GroupBox();
		this->pictureGroupBox = gcnew System::Windows::Forms::GroupBox();
		this->chatButtonGroupBox = gcnew System::Windows::Forms::GroupBox();


		//MainButtons
		this->cmd_Settings = gcnew System::Windows::Forms::Button();
		this->cmd_Map = gcnew System::Windows::Forms::Button();
		this->cmd_UpdateBlackboard = gcnew System::Windows::Forms::Button();
		this->cmd_Chat = gcnew System::Windows::Forms::Button();
		this->cmd_Picture = gcnew System::Windows::Forms::Button();
		this->cmd_LoginLogOut = gcnew System::Windows::Forms::Button();
		this->cmd_Draw = gcnew System::Windows::Forms::Button();
		this->cmd_Write = gcnew System::Windows::Forms::Button();
		//Draw
		this->cmd_leaveDraw = gcnew System::Windows::Forms::Button();
		this->cmd_Undo = gcnew System::Windows::Forms::Button();
		this->cmd_selectDrawColor = gcnew System::Windows::Forms::Button();
		this->draw_PenSizeComboBox = gcnew System::Windows::Forms::ComboBox();
		//WriteForms
		this->cmd_leaveWrite = gcnew System::Windows::Forms::Button();
		this->cmd_SelectWriteColor = gcnew System::Windows::Forms::Button();
		this->cmd_AddNewTextField = gcnew System::Windows::Forms::Button();
		this->write_TextSizeComboBox = gcnew System::Windows::Forms::ComboBox();
		//PictureForms
		this->cmd_leavePicture = gcnew System::Windows::Forms::Button();
		this->cmd_AddPicture = gcnew System::Windows::Forms::Button();
		//Chat
		this->chat_RichTextBox = gcnew System::Windows::Forms::RichTextBox();
		this->chat_TextBox = gcnew System::Windows::Forms::TextBox();
		this->cmd_leaveChat = gcnew System::Windows::Forms::Button();
		this->cmd_TextChatColorDialog = gcnew System::Windows::Forms::Button();
		this->cmd_NameChatColorDialog = gcnew System::Windows::Forms::Button();
		this->chat_TextSizeComboBox = gcnew System::Windows::Forms::ComboBox();
		
		this->buttonsGroupBox->SuspendLayout();
		this->drawButtonGroupBox->SuspendLayout();
		this->writeGroupBox->SuspendLayout();
		this->pictureGroupBox->SuspendLayout();
		this->chatButtonGroupBox->SuspendLayout();
		this->SuspendLayout();
	//Define the Difffernt Shapes
		//========================================== PANELES AND GROUPS ======================================
		// 
		// BlackBoardPanel
		// 
		this->blackBoardPanel->AllowDrop = true;
		this->blackBoardPanel->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
			| System::Windows::Forms::AnchorStyles::Left)
			| System::Windows::Forms::AnchorStyles::Right));
		this->blackBoardPanel->AutoSize = true;
		this->blackBoardPanel->BackColor = System::Drawing::SystemColors::ControlDarkDark;
		this->blackBoardPanel->Location = System::Drawing::Point(71, 11);
		this->blackBoardPanel->Name = L"BlackBoardPanel";
		this->blackBoardPanel->Size = System::Drawing::Size(733, 542);
		this->blackBoardPanel->TabIndex = 0;
		// 
		// ButtonsGroupBox
		// 
		this->buttonsGroupBox->Controls->Add(this->cmd_Settings);
		this->buttonsGroupBox->Controls->Add(this->cmd_Map);
		this->buttonsGroupBox->Controls->Add(this->cmd_UpdateBlackboard);
		this->buttonsGroupBox->Controls->Add(this->cmd_Chat);
		this->buttonsGroupBox->Controls->Add(this->cmd_Picture);
		this->buttonsGroupBox->Controls->Add(this->cmd_LoginLogOut);
		this->buttonsGroupBox->Controls->Add(this->cmd_Draw);
		this->buttonsGroupBox->Controls->Add(this->cmd_Write);
		this->buttonsGroupBox->Location = System::Drawing::Point(6, 11);
		this->buttonsGroupBox->Name = L"ButtonsGroupBox";
		this->buttonsGroupBox->Size = System::Drawing::Size(59, 542);
		this->buttonsGroupBox->TabIndex = 1;
		this->buttonsGroupBox->TabStop = false;
		//
		// DrawButtonGroupBox
		//
		this->drawButtonGroupBox->Controls->Add(this->cmd_leaveDraw);
		this->drawButtonGroupBox->Controls->Add(this->cmd_selectDrawColor);
		this->drawButtonGroupBox->Controls->Add(this->draw_PenSizeComboBox);
		this->drawButtonGroupBox->Controls->Add(this->cmd_Undo);
		this->drawButtonGroupBox->Location = System::Drawing::Point(6, 11);
		this->drawButtonGroupBox->Name = L"DrawButtonGroupBox";
		this->drawButtonGroupBox->Size = System::Drawing::Size(59, 542);
		this->drawButtonGroupBox->TabIndex = 2;
		this->drawButtonGroupBox->TabStop = false;
		this->drawButtonGroupBox->Hide();
		//
		// WriteTextGroupBox
		//
		this->writeGroupBox->Controls->Add(this->cmd_leaveWrite);
		this->writeGroupBox->Controls->Add(this->cmd_SelectWriteColor);
		this->writeGroupBox->Controls->Add(this->cmd_AddNewTextField);
		this->writeGroupBox->Controls->Add(this->write_TextSizeComboBox);
		this->writeGroupBox->Location = System::Drawing::Point(6, 11);
		this->writeGroupBox->Name = L"WriteTextGroupBox";
		this->writeGroupBox->Size = System::Drawing::Size(59, 542);
		this->writeGroupBox->TabIndex = 3;
		this->writeGroupBox->TabStop = false;
		this->writeGroupBox->Hide();
		//
		// pictureGroupBox
		//
		this->pictureGroupBox->Controls->Add(this->cmd_AddPicture);
		this->pictureGroupBox->Controls->Add(this->cmd_leavePicture);
		this->pictureGroupBox->Location = System::Drawing::Point(6, 11);
		this->pictureGroupBox->Name = L"PictureButtonGroupBox";
		this->pictureGroupBox->Size = System::Drawing::Size(59, 542);
		this->pictureGroupBox->TabIndex = 4;
		this->pictureGroupBox->TabStop = false;
		this->pictureGroupBox->Hide();
		//
		// ChatButtonGroupBox
		//
		this->chatButtonGroupBox->Controls->Add(this->cmd_leaveChat);
		this->chatButtonGroupBox->Controls->Add(this->cmd_TextChatColorDialog);
		this->chatButtonGroupBox->Controls->Add(this->cmd_NameChatColorDialog);
		this->chatButtonGroupBox->Controls->Add(this->chat_TextSizeComboBox);
		this->chatButtonGroupBox->Location = System::Drawing::Point(6, 11);
		this->chatButtonGroupBox->Name = L"ChatButtonGroupBox";
		this->chatButtonGroupBox->Size = System::Drawing::Size(59, 542);
		this->chatButtonGroupBox->TabIndex = 5;
		this->chatButtonGroupBox->TabStop = false;
		this->chatButtonGroupBox->Hide();
		//========================================= GENERAL BUTTONS =========================================
		// 
		// cmd_LoginLogOut
		// 
		this->cmd_LoginLogOut->Location = System::Drawing::Point(0, 29);
		this->cmd_LoginLogOut->Name = L"LoginLogOut";
		this->cmd_LoginLogOut->Size = System::Drawing::Size(buttonWidth, buttonHeight);
		this->cmd_LoginLogOut->TabIndex = 0;
		this->cmd_LoginLogOut->Text = L"LogIn";
		this->cmd_LoginLogOut->UseVisualStyleBackColor = true;
		this->cmd_LoginLogOut->Click += gcnew System::EventHandler(this, &MainBlackBoard::cmd_LoginLogOut_Click);
		// 
		// cmd_Chat
		// 
		this->cmd_Chat->Enabled = false;
		this->cmd_Chat->Location = System::Drawing::Point(0, 337);
		this->cmd_Chat->Name = L"cmd_Chat";
		this->cmd_Chat->Size = System::Drawing::Size(buttonWidth, buttonHeight);
		this->cmd_Chat->TabIndex = 4;
		this->cmd_Chat->Text = L"Open Chat";
		this->cmd_Chat->UseVisualStyleBackColor = true;
		this->cmd_Chat->Click += gcnew System::EventHandler(this, &MainBlackBoard::cmd_Chat_Click);
		// 
		// cmd_Draw
		// 
		this->cmd_Draw->Enabled = false;
		this->cmd_Draw->Location = System::Drawing::Point(0, 147);
		this->cmd_Draw->Name = L"cmd_Draw";
		this->cmd_Draw->Size = System::Drawing::Size(buttonWidth, buttonHeight);
		this->cmd_Draw->TabIndex = 1;
		this->cmd_Draw->Text = L"Draw";
		this->cmd_Draw->UseVisualStyleBackColor = true;
		this->cmd_Draw->Click += gcnew System::EventHandler(this, &MainBlackBoard::cmd_Draw_click);
		// 
		// cmd_Write
		// 
		this->cmd_Write->Enabled = false;
		this->cmd_Write->Location = System::Drawing::Point(0, 196);
		this->cmd_Write->Name = L"cmd_Write";
		this->cmd_Write->Size = System::Drawing::Size(buttonWidth, buttonHeight);
		this->cmd_Write->TabIndex = 2;
		this->cmd_Write->Text = L"Write";
		this->cmd_Write->UseVisualStyleBackColor = true;
		this->cmd_Write->Click += gcnew System::EventHandler(this, &MainBlackBoard::cmd_Write_Click);
		// 
		// cmd_Picture
		// 
		this->cmd_Picture->Enabled = false;
		this->cmd_Picture->Location = System::Drawing::Point(0, 245);
		this->cmd_Picture->Name = L"cmd_Picture";
		this->cmd_Picture->Size = System::Drawing::Size(buttonWidth, buttonHeight);
		this->cmd_Picture->TabIndex = 3;
		this->cmd_Picture->Text = L"Picture";
		this->cmd_Picture->UseVisualStyleBackColor = true;
		this->cmd_Picture->Click += gcnew System::EventHandler(this, &MainBlackBoard::cmd_Picture_Click);
		// 
		// cmd_SaveBlackboard
		// 
		this->cmd_UpdateBlackboard->Enabled = false;
		this->cmd_UpdateBlackboard->Location = System::Drawing::Point(0, 386);
		this->cmd_UpdateBlackboard->Name = L"cmd_SaveBlackboard";
		this->cmd_UpdateBlackboard->Size = System::Drawing::Size(buttonWidth, buttonHeight);
		this->cmd_UpdateBlackboard->TabIndex = 5;
		this->cmd_UpdateBlackboard->Text = L"Update Blackboard";
		this->cmd_UpdateBlackboard->UseVisualStyleBackColor = true;
		this->cmd_UpdateBlackboard->Click += gcnew System::EventHandler(this, &MainBlackBoard::cmd_UpdateBlackboard_Click);
		// 
		// cmd_Map
		// 
		this->cmd_Map->Enabled = false;
		this->cmd_Map->Location = System::Drawing::Point(0, 435);
		this->cmd_Map->Name = L"cmd_Map";
		this->cmd_Map->Size = System::Drawing::Size(buttonWidth, buttonHeight);
		this->cmd_Map->TabIndex = 7;
		this->cmd_Map->Text = L"Map";
		this->cmd_Map->UseVisualStyleBackColor = true;
		// 
		// cmd_Settings
		// 
		this->cmd_Settings->Enabled = false;
		this->cmd_Settings->Location = System::Drawing::Point(0, 499);
		this->cmd_Settings->Name = L"cmd_Settings";
		this->cmd_Settings->Size = System::Drawing::Size(buttonWidth, buttonHeight);
		this->cmd_Settings->TabIndex = 8;
		this->cmd_Settings->Text = L"Settings";
		this->cmd_Settings->UseVisualStyleBackColor = true;
		//========================================= DRAWING ==================================================
		// 
		// cmd_leaveDraw
		// 
		this->cmd_leaveDraw->Enabled = true;
		this->cmd_leaveDraw->Location = System::Drawing::Point(0, 29);
		this->cmd_leaveDraw->Name = L"cmd_LeaveDraw";
		this->cmd_leaveDraw->Size = System::Drawing::Size(buttonWidth, buttonHeight);
		this->cmd_leaveDraw->TabIndex = 0;
		this->cmd_leaveDraw->Text = L"Back";
		this->cmd_leaveDraw->UseVisualStyleBackColor = true;
		this->cmd_leaveDraw->Click += gcnew System::EventHandler(this, &MainBlackBoard::cmd_Draw_click);
		// 
		// cmd_undo
		// 
		this->cmd_Undo->Enabled = true;
		this->cmd_Undo->Location = System::Drawing::Point(0, 82);
		this->cmd_Undo->Name = L"cmd_LeaveDraw";
		this->cmd_Undo->Size = System::Drawing::Size(buttonWidth, buttonHeight);
		this->cmd_Undo->TabIndex = 0;
		this->cmd_Undo->Text = L"Undo";
		this->cmd_Undo->UseVisualStyleBackColor = true;
		this->cmd_Undo->Click += gcnew System::EventHandler(this, &MainBlackBoard::cmd_Undo_click);
		// 
		// cmd_selectDrawColor
		// 
		this->cmd_selectDrawColor->Enabled = true;
		this->cmd_selectDrawColor->Location = System::Drawing::Point(0, 337);
		this->cmd_selectDrawColor->Name = L"cmd_Chat";
		this->cmd_selectDrawColor->Size = System::Drawing::Size(buttonWidth, buttonHeight);
		this->cmd_selectDrawColor->TabIndex = 1;
		this->cmd_selectDrawColor->Text = L"";
		this->cmd_selectDrawColor->UseVisualStyleBackColor = true;
		this->cmd_selectDrawColor->BackColor = System::Drawing::Color::Black;
		this->cmd_selectDrawColor->Click += gcnew System::EventHandler(this, &MainBlackBoard::cmd_selectDrawColor_click);
		//
		//	Draw_SizeComboBox
		//
		this->draw_PenSizeComboBox->Enabled = true;
		this->draw_PenSizeComboBox->Location = System::Drawing::Point(0, 390);
		this->draw_PenSizeComboBox->Name = "Draw_SizeComboBox";
		this->draw_PenSizeComboBox->Size = System::Drawing::Size(buttonWidth, buttonHeight);
		this->draw_PenSizeComboBox->TabIndex = 2;
		this->draw_PenSizeComboBox->SelectedIndexChanged += gcnew System::EventHandler(this, &MainBlackBoard::cmd_SelectedSizeChanged);
		//============================================== WRITE  =========================================================
		//
		// cmd_leaveWrite
		//
		this->cmd_leaveWrite->Enabled = true;
		this->cmd_leaveWrite->Location = System::Drawing::Point(0, 29);
		this->cmd_leaveWrite->Name = L"cmd_leave_Write";
		this->cmd_leaveWrite->Size = System::Drawing::Size(buttonWidth, buttonHeight);
		this->cmd_leaveWrite->TabIndex = 0;
		this->cmd_leaveWrite->Text = L"back";
		this->cmd_leaveWrite->UseVisualStyleBackColor = true;
		this->cmd_leaveWrite->Click += gcnew System::EventHandler(this, &MainBlackBoard::cmd_Write_Click);
		//
		// cmd_SelectWriteColor
		//
		this->cmd_SelectWriteColor->Enabled = true;
		this->cmd_SelectWriteColor->Location = System::Drawing::Point(0, 435);
		this->cmd_SelectWriteColor->Name = L"cmd_ChoseChatColorDialog";
		this->cmd_SelectWriteColor->Size = System::Drawing::Size(buttonWidth, buttonHeight);
		this->cmd_SelectWriteColor->TabIndex = 2;
		this->cmd_SelectWriteColor->Text = L"Color";
		this->cmd_SelectWriteColor->BackColor = System::Drawing::Color::Black;
		this->cmd_SelectWriteColor->UseVisualStyleBackColor = true;
		this->cmd_SelectWriteColor->Click += gcnew System::EventHandler(this, &MainBlackBoard::cmd_selectWriteColor_click);
		// 
		// cmd_ChangeSize
		// 
		this->write_TextSizeComboBox->Enabled = true;
		this->write_TextSizeComboBox->Location = System::Drawing::Point(0, 386);
		this->write_TextSizeComboBox->Name = L"cmd_ChoseWriteTextSize";
		this->write_TextSizeComboBox->Size = System::Drawing::Size(buttonWidth, buttonHeight);
		this->write_TextSizeComboBox->TabIndex = 3;
		this->write_TextSizeComboBox->Text = writeTextSize.ToString();
		this->write_TextSizeComboBox->SelectedIndexChanged += gcnew System::EventHandler(this, &MainBlackBoard::cmd_ChoseWriteSize_Change);
		//
		// cmd_AddNewTextField
		//
		this->cmd_AddNewTextField->Enabled = true;
		this->cmd_AddNewTextField->Location = System::Drawing::Point(0, 100);
		this->cmd_AddNewTextField->Name = L"cmd_AddNewTextField";
		this->cmd_AddNewTextField->Size = System::Drawing::Size(buttonWidth, buttonHeight);
		this->cmd_AddNewTextField->TabIndex = 4;
		this->cmd_AddNewTextField->Text = L"Add New Textfield";
		this->cmd_AddNewTextField->UseVisualStyleBackColor = true;
		this->cmd_AddNewTextField->Click += gcnew System::EventHandler(this, &MainBlackBoard::cmd_AddNewTextField_Click);
		//============================================== Picture =========================================================
		//
		// cmd_leavePicture
		//
		this->cmd_leavePicture->Enabled = true;
		this->cmd_leavePicture->Location = System::Drawing::Point(0, 29);
		this->cmd_leavePicture->Name = L"cmd_leavePicture";
		this->cmd_leavePicture->Size = System::Drawing::Size(buttonWidth, buttonHeight);
		this->cmd_leavePicture->TabIndex = 0;
		this->cmd_leavePicture->Text = L"back";
		this->cmd_leavePicture->UseVisualStyleBackColor = true;
		this->cmd_leavePicture->Click += gcnew System::EventHandler(this, &MainBlackBoard::cmd_Picture_Click);
		//
		// cmd_AddPicture
		//
		this->cmd_AddPicture->Enabled = true;
		this->cmd_AddPicture->Location = System::Drawing::Point(0, 100);
		this->cmd_AddPicture->Name = L"cmd_AddPicture";
		this->cmd_AddPicture->Size = System::Drawing::Size(buttonWidth, buttonHeight);
		this->cmd_AddPicture->TabIndex = 0;
		this->cmd_AddPicture->Text = L"Add New Picture";
		this->cmd_AddPicture->UseVisualStyleBackColor = true;
		this->cmd_AddPicture->Click += gcnew System::EventHandler(this, &MainBlackBoard::cmd_AddPicture_Click);

		//============================================== CHAT ================================================
		//
		// Chat_listBox
		//
		this->chat_RichTextBox->Location = System::Drawing::Point(71, 11);
		this->chat_RichTextBox->Size = System::Drawing::Size(733, 500);
		this->chat_RichTextBox->Anchor = blackBoardPanel->Anchor;
		this->chat_RichTextBox->BackColor = System::Drawing::Color::DarkSlateGray;
		this->chat_RichTextBox->Hide();
		this->chat_RichTextBox->TabIndex = 3;
		this->chat_RichTextBox->ReadOnly = true;
		//this->chat_RichTextBox->scroll
		this->chat_RichTextBox->Name = L"ChatListBox";
		//
		// Chat_TextBox
		//
		this->chat_TextBox->Location = System::Drawing::Point(chat_RichTextBox->Location.X, (chat_RichTextBox->Location.Y + chat_RichTextBox->Size.Height));
		this->chat_TextBox->Size = System::Drawing::Size(733, 60);
		this->chat_TextBox->Anchor = blackBoardPanel->Anchor;
		this->chat_TextBox->Hide();
		this->chat_TextBox->Name = L"ChatTextBox";
		this->chat_TextBox->TabIndex = 1;
		this->chat_TextBox->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &MainBlackBoard::chat_TextBox_KeyDown);
		// 
		// cmd_leaveChat
		// 
		this->cmd_leaveChat->Enabled = true;
		this->cmd_leaveChat->Location = System::Drawing::Point(0, 29);
		this->cmd_leaveChat->Name = L"cmd_leave_Chat";
		this->cmd_leaveChat->Size = System::Drawing::Size(buttonWidth, buttonHeight);
		this->cmd_leaveChat->TabIndex = 0;
		this->cmd_leaveChat->Text = L"close Chat";
		this->cmd_leaveChat->UseVisualStyleBackColor = true;
		this->cmd_leaveChat->Click += gcnew System::EventHandler(this, &MainBlackBoard::cmd_Chat_Click);
		
		//
		//	cmd_TextChatColorDialog
		//
		this->cmd_TextChatColorDialog->Enabled = true;
		this->cmd_TextChatColorDialog->Location = System::Drawing::Point(0, 435);
		this->cmd_TextChatColorDialog->Name = L"cmd_ChoseChatColorDialog";
		this->cmd_TextChatColorDialog->Size = System::Drawing::Size(buttonWidth, buttonHeight);
		this->cmd_TextChatColorDialog->TabIndex = 2;
		this->cmd_TextChatColorDialog->Text = L"Text Color";
		this->cmd_TextChatColorDialog->UseVisualStyleBackColor = true;
		this->cmd_TextChatColorDialog->Click += gcnew System::EventHandler(this, &MainBlackBoard::cmd_TextChatColorDialog_Click);
		//
		//	cmd_ChoseChatColorDialog
		//
		this->cmd_NameChatColorDialog->Enabled = true;
		this->cmd_NameChatColorDialog->Location = System::Drawing::Point(0, 490);
		this->cmd_NameChatColorDialog->Name = L"cmd_ChoseChatColorDialog";
		this->cmd_NameChatColorDialog->Size = System::Drawing::Size(buttonWidth, buttonHeight);
		this->cmd_NameChatColorDialog->TabIndex = 2;
		this->cmd_NameChatColorDialog->Text = L"Name Color";
		this->cmd_NameChatColorDialog->UseVisualStyleBackColor = true;
		this->cmd_NameChatColorDialog->Click += gcnew System::EventHandler(this, &MainBlackBoard::cmd_NameChatColorDialog_Click);

		// 
		// cmd_ChangeSize
		// 
		this->chat_TextSizeComboBox->Enabled = true;
		this->chat_TextSizeComboBox->Location = System::Drawing::Point(0, 386);
		this->chat_TextSizeComboBox->Name = L"cmd_ChoseChatTextSize";
		this->chat_TextSizeComboBox->Size = System::Drawing::Size(buttonWidth, buttonHeight);
		this->chat_TextSizeComboBox->TabIndex = 4;
		this->chat_TextSizeComboBox->Text = chatTextSize.ToString();
		this->chat_TextSizeComboBox->SelectedIndexChanged += gcnew System::EventHandler(this, &MainBlackBoard::cmd_ChoseChatSize_Change);
		
		//======================================== Spawning them all in ======================================
		// 
		// MainBlackBoard
		// 
		this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
		this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
		this->AutoScroll = true;
		this->ClientSize = System::Drawing::Size(816, 566);
		this->Controls->Add(this->buttonsGroupBox);
		this->Controls->Add(this->blackBoardPanel);
		this->Controls->Add(this->drawButtonGroupBox);
		this->Controls->Add(this->writeGroupBox);
		this->Controls->Add(this->pictureGroupBox);
		this->Controls->Add(this->chatButtonGroupBox);
		this->Controls->Add(this->chat_TextBox);
		this->Controls->Add(this->chat_RichTextBox);
		this->Name = L"MainBlackBoard";
		this->Text = L"MainBlackBoard";
		this->buttonsGroupBox->ResumeLayout(false);
		this->buttonsGroupBox->PerformLayout();
		this->drawButtonGroupBox->ResumeLayout(false);
		this->drawButtonGroupBox->PerformLayout();
		this->writeGroupBox->ResumeLayout(false);
		this->writeGroupBox->PerformLayout();
		this->pictureGroupBox->ResumeLayout(false);
		this->pictureGroupBox->PerformLayout();
		this->chatButtonGroupBox->ResumeLayout(false);
		this->chatButtonGroupBox->PerformLayout();
		this->blackBoardPanel->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &MainBlackBoard::blackBoard_MouseDown);
		this->blackBoardPanel->MouseUp += gcnew System::Windows::Forms::MouseEventHandler(this, &MainBlackBoard::blackBoard_MouseUp);
		this->blackBoardPanel->MouseMove += gcnew System::Windows::Forms::MouseEventHandler(this, &MainBlackBoard::blackBoard_MouseMove);
		this->ResumeLayout(false);
		this->PerformLayout();

	}
#pragma endregion

///========================================================= FUNCTIONS ========================================

//======================================================= DRAW FUNCTIONS =================================================

#pragma region Draw Functions
//Opens and Closes the Draw Menu.
private: System::Void cmd_Draw_click(System::Object^  sender, System::EventArgs^  e)
{
	if (this->drawOpend == false)
	{
		this->buttonsGroupBox->Hide();
		this->drawButtonGroupBox->Show();
		this->drawOpend = true;
		sizeCheck = drawPointsSteeps->Count;
	}
	else
	{
		this->buttonsGroupBox->Show();
		this->drawButtonGroupBox->Hide();
		this->drawOpend = false;
		if (sizeCheck != drawPointsSteeps->Count)
		{
			blackBoardApiClass->updateDrawing(drawPointsSteeps, drawColorSteeps, drawSizeSteeps, blackBoardName);
		}		
	}
}

//Undo the last steep (buggy at the moment)
private: System::Void cmd_Undo_click(System::Object^ sender, System::EventArgs^ e)
{
	//
	List<Point>^ tempPointList = gcnew List<Point>();
	int steepCount = this->drawPointsSteeps->Count - 1;
	if (steepCount < 0)
	{
		return;
	}
	tempPointList = this->drawPointsSteeps[steepCount];
	this->drawPointsSteeps->Remove(tempPointList);
	this->drawColorSteeps->Remove(this->drawColorSteeps[steepCount]);
	this->drawSizeSteeps->Remove(this->drawSizeSteeps[steepCount]);
	drawAllSteeps();
}
private: void drawAllSteeps()
{
	this->blackBoardPanel->Refresh();
	int steepCount = this->drawPointsSteeps->Count;
	if (steepCount == 0)
	{
		return;
	}
	int i = 0;
	while (i < steepCount)
	{
		drawSteeps(drawPointsSteeps[i], drawColorSteeps[i], drawSizeSteeps[i]);
		i++;
	}
}


private: void drawSteeps(List<Point>^ _drawpoints, Color _Color, int _PenSize)
{
	List<Point>^ tempDrawPoints = gcnew List<Point>();
	for each (Point drawpoint in _drawpoints)
	{
		Pen^ pen = gcnew Pen(_Color, _PenSize);
		tempDrawPoints->Add(drawpoint);
		if (tempDrawPoints->Count > 1)
		{
			Graphics ^g = this->blackBoardPanel->CreateGraphics();
			g->DrawLines(pen, tempDrawPoints->ToArray());
		}
	}
}

//sets the first point for the new drawing
private: void draw_MouseDown(System::Windows::Forms::MouseEventArgs^  e)
{
	if (e->Button == System::Windows::Forms::MouseButtons::Left)
	{
		this->drawpointList->Add(e->Location);
	}
}

//adds a copy of the current Pointlist to the steeps and clears the list
private: void draw_MouseUp(System::Windows::Forms::MouseEventArgs^ e)
{
	if (e->Button == System::Windows::Forms::MouseButtons::Left)
	{
		List<Point>^ tmpList = gcnew List<Point>();
		for each (Point point in this->drawpointList)
		{
			tmpList->Add(point);
		}
		this->drawPointsSteeps->Add(tmpList);
		this->drawColorSteeps->Add(cmd_selectDrawColor->BackColor);
		this->drawSizeSteeps->Add(penSize);
		this->drawpointList->Clear();
	}
}

//The actually Draw function every movement of the mouse adds a point to a list and draws a line betwen them
private: void draw_MouseMove(System::Windows::Forms::MouseEventArgs^  e)
{

	if (e->Button == System::Windows::Forms::MouseButtons::Left)
	{
		this->drawpointList->Add(e->Location);
		drawing();
	}
}
private: void drawing()
{
	Pen^ pen = gcnew Pen(penColor, penSize);
	if (drawpointList->Count > 1)
	{
		Graphics ^g = this->blackBoardPanel->CreateGraphics();
		g->DrawLines(pen, this->drawpointList->ToArray());
		delete g;
	}
}
//changes the color of the selectcolorButton with a dialog Menu The color is then used to draw the next line
private: System::Void cmd_selectDrawColor_click(System::Object^  sender, System::EventArgs^  e)
{
	ColorDialog^ MyDialog = gcnew ColorDialog;
	// Keeps the user from selecting a custom color.
	MyDialog->AllowFullOpen = true;
	// Allows the user to get help. (The default is false.)
	MyDialog->ShowHelp = true;
	// Sets the initial color select to the current text color.
	MyDialog->Color = this->cmd_selectDrawColor->BackColor;

	// Update the text box color if the user clicks OK 
	if (MyDialog->ShowDialog() == ::System::Windows::Forms::DialogResult::OK)
	{
		this->cmd_selectDrawColor->BackColor = MyDialog->Color;
		this->penColor = MyDialog->Color;
	}
}

//Gets called if the SizeComboBox is used to select a new size. Assigns the size to the PenSize Value
private: System::Void cmd_SelectedSizeChanged(System::Object^ sender, System::EventArgs^ e)
{
	this->penSize = Convert::ToInt32(draw_PenSizeComboBox->SelectedItem);
}
#pragma endregion

//================================================== WRITE FUNCTIONS ====================================================

#pragma region Write Functions
//Opens and closes the Write Menu. Unlocks alredy exisiting text but only if the Displayname is corrects
private: System::Void cmd_Write_Click(System::Object^ sender, System::EventArgs^ e)
{
	if (this->writeOpend == false)
	{
		this->buttonsGroupBox->Hide();
		this->writeGroupBox->Show();
		this->writeOpend = true;
		sizeCheck = writeRichTextBoxList->Count;
		for each (RichTextBox^ textBox in writeRichTextBoxList)
		{
			if (textBox->Name == displayName)
			{
				textBox->ReadOnly = false;
				for each (Control^ control in textBox->Controls)
				{
					control->Show();
				}
			}
		}
	}
	else
	{
		this->buttonsGroupBox->Show();
		this->writeGroupBox->Hide();
		this->writeOpend = false;
		if (sizeCheck != writeRichTextBoxList->Count)
		{
			blackBoardApiClass->updateTextBoxes(writeRichTextBoxList, blackBoardName);
		}
		for each (RichTextBox^ textBox in writeRichTextBoxList)
		{
			textBox->ReadOnly = true;
			for each (Control^ control in textBox->Controls)
			{
				control->Hide();
			}
		}

	}
}

private: System::Void cmd_AddNewTextField_Click(System::Object^ sender, System::EventArgs^ e)
{
	currentSelectedTextBox = nullptr;
}

//reacts if the key was pressed down on the blackboard while the write menu is activated gets Called from the Blackboard_MouseDown
private: void write_MouseDown(System::Windows::Forms::MouseEventArgs^  e)
{
	if (currentSelectedTextBox != nullptr)
	{
		return;
	}

	if (e->Button == System::Windows::Forms::MouseButtons::Left)
	{
		RichTextBox^ write_RichTextBox = gcnew System::Windows::Forms::RichTextBox();

		write_RichTextBox->Location = e->Location;
		write_RichTextBox->Size = System::Drawing::Size(100, 80);
		write_RichTextBox->Name = this->displayName;
		write_RichTextBox->ForeColor = this->writeColor;
		write_RichTextBox->BorderStyle = System::Windows::Forms::BorderStyle::None;
		write_RichTextBox->Focus();
		Control^ target = static_cast<Control^>(write_RichTextBox);
		this->addScaleImage(target);
		this->addCloseImage(target);
		this->addMoveBar(target);
		write_RichTextBox->Font = gcnew System::Drawing::Font(write_RichTextBox->Font->Name, this->writeTextSize, write_RichTextBox->Font->Style, write_RichTextBox->Font->Unit);
		this->blackBoardPanel->Controls->Add(write_RichTextBox);
		this->currentSelectedTextBox = write_RichTextBox;
		this->writeRichTextBoxList->Add(write_RichTextBox);
	}
}


private: System::Void cmd_ChoseWriteSize_Change(System::Object^  sender, System::EventArgs^  e)
{
	this->writeTextSize = Convert::ToInt32(write_TextSizeComboBox->SelectedItem);	
	this->currentSelectedTextBox->Font = gcnew System::Drawing::Font(this->currentSelectedTextBox->Font->Name, this->writeTextSize, this->currentSelectedTextBox->Font->Style, this->currentSelectedTextBox->Font->Unit);
}

private: System::Void cmd_selectWriteColor_click(System::Object^  sender, System::EventArgs^  e)
{
	ColorDialog^ MyDialog = gcnew ColorDialog;
	// Keeps the user from selecting a custom color.
	MyDialog->AllowFullOpen = true;
	// Allows the user to get help. (The default is false.)
	MyDialog->ShowHelp = true;
	// Sets the initial color select to the current text color.
	MyDialog->Color = this->cmd_SelectWriteColor->BackColor;

	// Update the text box color if the user clicks OK 
	if (MyDialog->ShowDialog() == ::System::Windows::Forms::DialogResult::OK)
	{
		this->cmd_SelectWriteColor->BackColor = MyDialog->Color;
		this->writeColor = MyDialog->Color;
		if (currentSelectedTextBox != nullptr)
		{
			currentSelectedTextBox->BackColor = currentSelectedTextBox->BackColor;
			currentSelectedTextBox->ForeColor = this->writeColor;
		}
	}
}

private: System::Void selectCurrentWindow_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
{
	if (e->Button != System::Windows::Forms::MouseButtons::Left)
	{
		return;
	}
	RichTextBox^ tmpBox = static_cast<RichTextBox^>(sender);
	if (tmpBox->Name == displayName)
	{
		this->currentSelectedTextBox = tmpBox;
	}
}
#pragma endregion

//================================================= PICTURE FUNCTIONS ===================================================

#pragma region Picture Functions
private: System::Void cmd_Picture_Click(System::Object^  sender, System::EventArgs^  e)
{
	if (this->pictureOpend == false)
	{
		this->buttonsGroupBox->Hide();
		this->pictureGroupBox->Show();
		this->pictureOpend = true;
		sizeCheck = pictureList->Count;
		for each (PictureBox^ picture in pictureList)
		{
			if (picture->Name == displayName)
			{
				for each (Control^ control in picture->Controls)
				{
					control->Show();
				}
			}
		}
	}
	else
	{
		this->buttonsGroupBox->Show();
		this->pictureGroupBox->Hide();
		this->pictureOpend = false;
		if (sizeCheck != pictureList->Count)
		{
			blackBoardApiClass->updatePicture(pictureList, blackBoardName);
		}
		for each (PictureBox^ picture in pictureList)
		{
			for each (Control^ control in picture->Controls)
			{
				control->Hide();
			}
		}
	}
}

private: System::Void cmd_AddPicture_Click(System::Object^  sender, System::EventArgs^  e)
{
	
	fileExplorer->ShowDialog();
	if (fileExplorer->getImage() != nullptr)
	{
		selectedImage = fileExplorer->getImage();
		this->blackBoardPanel->Cursor = System::Windows::Forms::Cursors::Cross;
	}
	else
	{
		Console::WriteLine("Nay");
	}
}

private: void picture_MouseDown(System::Windows::Forms::MouseEventArgs^  e)
{
	if (selectedImage == nullptr)
	{
		return;
	}

	PictureBox^ picture = gcnew System::Windows::Forms::PictureBox();
	picture->Name = displayName;
	picture->Image = selectedImage;
	picture->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
	picture->Size = System::Drawing::Size(120,120);
	picture->Cursor = System::Windows::Forms::Cursors::Arrow;
	picture->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
	picture->TabStop = 1;
	picture->TabStop = false;
	picture->Location = System::Drawing::Point(e->X, e->Y);
	addCloseImage(picture);
	addScaleImage(picture);
	addMoveBar(picture);
	blackBoardPanel->Controls->Add(picture);
	pictureList->Add(picture);

	selectedImage = nullptr;
	blackBoardPanel->Cursor = System::Windows::Forms::Cursors::Arrow;
}

#pragma endregion

//================================================== CHAT FUNCTIONS =====================================================

#pragma region Chat Functions
//Opens and closes the chat by showing/hiding everything
private: System::Void cmd_Chat_Click(System::Object^  sender, System::EventArgs^  e)
{
	if (this->chatOpend == false)
	{
		this->blackBoardPanel->Hide();
		this->buttonsGroupBox->Hide();
		this->chat_RichTextBox->Show();
		this->chat_TextBox->Show();
		this->chatButtonGroupBox->Show();
		this->chatOpend = true;
	}
	else
	{
		this->buttonsGroupBox->Show();
		this->blackBoardPanel->Show();
		this->chat_RichTextBox->Hide();
		this->chat_TextBox->Hide();
		this->chatButtonGroupBox->Hide();
		this->chatOpend = false;
	}
}

//Sets the color of the Text and Date in the Chat by offering a dialog. The color is displayed on the colorButton
private: System::Void cmd_TextChatColorDialog_Click(System::Object^  sender, System::EventArgs^  e)
{
	ColorDialog^ MyDialog = gcnew ColorDialog;
	// Keeps the user from selecting a custom color.
	MyDialog->AllowFullOpen = true;
	// Allows the user to get help. (The default is false.)
	MyDialog->ShowHelp = true;
	// Sets the initial color select to the current text color.
	MyDialog->Color = this->chat_TextBox->ForeColor;

	// Update the text box color if the user clicks OK 
	if (MyDialog->ShowDialog() == ::System::Windows::Forms::DialogResult::OK)
	{
		this->chat_TextBox->ForeColor = MyDialog->Color;
		this->cmd_TextChatColorDialog->ForeColor = MyDialog->Color;
	}
}

//Sets the color of the Name in the Chat by offering a dialog. The color is displayed on the colorButton
private: System::Void cmd_NameChatColorDialog_Click(System::Object^  sender, System::EventArgs^  e)
{
	ColorDialog^ MyDialog = gcnew ColorDialog;
	// Keeps the user from selecting a custom color.
	MyDialog->AllowFullOpen = true;
	// Allows the user to get help. (The default is false.)
	MyDialog->ShowHelp = true;
	// Sets the initial color select to the current text color.
	MyDialog->Color = this->nameColor;

	// Update the text box color if the user clicks OK 
	if (MyDialog->ShowDialog() == ::System::Windows::Forms::DialogResult::OK)
	{
		this->nameColor = MyDialog->Color;
		this->cmd_NameChatColorDialog->ForeColor = MyDialog->Color;
	}
}

//Inserts the text from the Textbox into the Chatfield(The RichtTextBox) 
private: System::Void chat_TextBox_KeyDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e)
{
	if (e->KeyCode == Keys::Return)
	{
		DateTime localDate = DateTime::Now;		
		int pos = chat_RichTextBox->TextLength;
		String^ dateText = localDate.Hour.ToString() + ":" + localDate.Minute.ToString() + " ";
		this->chat_RichTextBox->AppendText(dateText);
		this->chat_RichTextBox->Select(pos, dateText->Length);
		this->chat_RichTextBox->SelectionColor = this->chat_TextBox->ForeColor;
		this->chat_RichTextBox->SelectionFont = this->chat_TextBox->Font;
		pos = chat_RichTextBox->TextLength;
		String^ nameText = this->displayName + ": ";
		this->chat_RichTextBox->AppendText(nameText);
		this->chat_RichTextBox->Select(pos, nameText->Length);
		this->chat_RichTextBox->SelectionColor = nameColor;
		this->chat_RichTextBox->SelectionFont = this->chat_TextBox->Font;
		pos = chat_RichTextBox->TextLength;
		String^ chatText = this->chat_TextBox->Text + "\n";
		this->chat_RichTextBox->AppendText(chatText);
		this->chat_RichTextBox->Select(pos, chatText->Length);
		this->chat_RichTextBox->SelectionColor = this->chat_TextBox->ForeColor;
		this->chat_RichTextBox->SelectionFont = this->chat_TextBox->Font;
		this->chat_TextBox->Text = "";

		blackBoardApiClass->updateChat(chat_RichTextBox->Text,blackBoardName);
	}
}

//Sets the Size of the TextChat from 10 to 29((defined in the standart Constructor))
private: System::Void cmd_ChoseChatSize_Change(System::Object^  sender, System::EventArgs^  e)
{
	this->chatTextSize = Convert::ToInt32(chat_TextSizeComboBox->SelectedItem);
	this->chat_TextBox->Font = gcnew System::Drawing::Font(this->chat_TextBox->Font->Name, this->chatTextSize, this->chat_TextBox->Font->Style, this->chat_TextBox->Font->Unit);
}
#pragma endregion
//================================================= SYSTEM FUNCTIONS ====================================================

#pragma region SYSTEM FUNCTIONS
//================================================ UPDATE BLACK BOARD ===================================================
private: System::Void cmd_UpdateBlackboard_Click(System::Object^  sender, System::EventArgs^  e)
{
	updateBoard();
}
//=========================================== BLACK BOARD MOUSE FUNCTIONS ===============================================
//chose which function of the blackboardMouseDown is run
private: System::Void blackBoard_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
{
	if (this->drawOpend == true)
	{
		this->draw_MouseDown(e);
		return;
	}
	if (this->writeOpend == true)
	{
		this->write_MouseDown(e);
		return;
	}
	if (this->pictureOpend == true)
	{
		this->picture_MouseDown(e);
		return;
	}
}


//chose which function of the blackboardMouseUp is run
private: System::Void blackBoard_MouseUp(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
{
	if (drawOpend == true)
	{
		draw_MouseUp(e);
		return;
	}


}

//chose which function of the blackboardMouseMove is run
private: System::Void blackBoard_MouseMove(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
{
	if (drawOpend == true)
	{
		draw_MouseMove(e);
		return;
	}

}

//================================================ ADD SCALE / MOVE / CLOSE ===============================================

private: void addScaleImage(Control^ target)
{
	PictureBox^ scaleImage = gcnew System::Windows::Forms::PictureBox();
	scaleImage->Image = gcnew Bitmap("ResizeTexture.png");
	scaleImage->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
	scaleImage->Cursor = System::Windows::Forms::Cursors::SizeAll;
	scaleImage->Size = System::Drawing::Size(16, 16);
	scaleImage->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
	scaleImage->TabStop = 1;
	scaleImage->TabStop = false;
	scaleImage->Location = System::Drawing::Point(target->Width - scaleImage->Width-4, target->Height - scaleImage->Height);
	scaleImage->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &MainBlackBoard::resizeWindow_MouseDown);
	scaleImage->MouseMove += gcnew System::Windows::Forms::MouseEventHandler(this, &MainBlackBoard::resizeWindow_MouseMove);
	scaleImage->MouseUp += gcnew System::Windows::Forms::MouseEventHandler(this, &MainBlackBoard::resizeWindow_MouseUp);
	target->Controls->Add(scaleImage);
}

private: System::Void resizeWindow_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
{
	if (e->Button != System::Windows::Forms::MouseButtons::Left)
	{
		return;
	}
	mouseClick = true;
}

//Resizes the Window towards where the mouse when clicked
private: System::Void resizeWindow_MouseMove(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
{
	if (e->Button != System::Windows::Forms::MouseButtons::Left)
	{
		return;
	}
	if (mouseClick == true)
	{
		PictureBox^ write_ScaleImage = dynamic_cast<PictureBox^>(sender);
		Control^ parentControl = write_ScaleImage->Parent;
		int newParentHeight = write_ScaleImage->Top + e->Y;
		int newParentWidth = write_ScaleImage->Left + e->X;
		parentControl->Height = newParentHeight;
		parentControl->Width = newParentWidth;
	}
}

//Does one final resize of the window
private: System::Void resizeWindow_MouseUp(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
{
	if (e->Button != System::Windows::Forms::MouseButtons::Left)
	{
		return;
	}
	if (mouseClick == true)
	{
		if (writeOpend == true)
		{
			PictureBox^ write_ScaleImage = dynamic_cast<PictureBox^>(sender);
			Control^ parentControl = write_ScaleImage->Parent;
			int newParentHeight = write_ScaleImage->Top + e->Y;
			int newParentWidth = write_ScaleImage->Left + e->X;


			if (newParentHeight < 30)
			{
				newParentHeight = 31;
			}

			if (parentControl->Width < 30)
			{
				newParentWidth = 31;
			}

			parentControl->Height = newParentHeight;
			parentControl->Width = newParentWidth;
		}
	}
	mouseClick = false;
}

//Adds a X to the upper right corner of the form
private: void addCloseImage(Control^ target)
{
	PictureBox^ closeImage = gcnew System::Windows::Forms::PictureBox();
	closeImage->Image = gcnew Bitmap("CloseTexture.png");
	closeImage->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
	closeImage->Size = System::Drawing::Size(12, 12);
	closeImage->Cursor = System::Windows::Forms::Cursors::Arrow;
	closeImage->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
	closeImage->TabStop = 1;
	closeImage->TabStop = false;
	closeImage->Location = System::Drawing::Point(target->Width - 16, 0);
	closeImage->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &MainBlackBoard::closeWindow_MouseDown);
	target->Controls->Add(closeImage);
}


//Removes the form from the BlackBoardPanel And out of the List of the Object
private: System::Void closeWindow_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
{
	if (e->Button != System::Windows::Forms::MouseButtons::Left)
	{
		return;
	}
	PictureBox^ closeImage = dynamic_cast<PictureBox^>(sender);
	Control^ parentControl = closeImage->Parent;
	if (writeOpend == true)
	{
		int counter = 0;
		for each (Control^ con in writeRichTextBoxList)
		{
			if (con == parentControl)
			{
				break;
			}
			counter++;
		}
		writeRichTextBoxList->Remove(writeRichTextBoxList[counter]);
	}
	if (pictureOpend == true)
	{
		int counter = 0;
		for each (Control^ con in pictureList)
		{
			if (con == parentControl)
			{
				break;
			}
			counter++;
		}
		pictureList->Remove(pictureList[counter]);
	}
	this->blackBoardPanel->Controls->Remove(parentControl);
	drawAllSteeps();
}

private: void addMoveBar(Control^ target)
{
	PictureBox^ moveBarImage = gcnew System::Windows::Forms::PictureBox();
	moveBarImage->Image = gcnew Bitmap("MoveBarTexture.png");
	moveBarImage->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top )| System::Windows::Forms::AnchorStyles::Left)| System::Windows::Forms::AnchorStyles::Right));
	moveBarImage->Size = System::Drawing::Size(target->Width, 10);
	moveBarImage->Cursor = System::Windows::Forms::Cursors::Hand;
	moveBarImage->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
	moveBarImage->TabStop = 2;
	moveBarImage->TabStop = false;
	moveBarImage->Location = System::Drawing::Point(0, 0);
	moveBarImage->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &MainBlackBoard::moveBarWindow_MouseDown);
	moveBarImage->MouseMove += gcnew System::Windows::Forms::MouseEventHandler(this, &MainBlackBoard::moveBarWindow_MouseMove);
	moveBarImage->MouseUp += gcnew System::Windows::Forms::MouseEventHandler(this, &MainBlackBoard::moveBarWindow_MouseUp);
	target->Controls->Add(moveBarImage);
}

private: System::Void moveBarWindow_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
{
	if (e->Button != System::Windows::Forms::MouseButtons::Left)
	{
		return;
	}
	moveOffsetX = e->X;
	moveOffsetY = e->Y;
	mouseClick = true;
}

//Moves the Window with the mouse when clicked
private: System::Void moveBarWindow_MouseMove(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
{
	if (e->Button != System::Windows::Forms::MouseButtons::Left)
	{
		return;
	}
	if (mouseClick == true)
	{
		PictureBox^ moveImage = dynamic_cast<PictureBox^>(sender);
		Control^ parentControl = moveImage->Parent;
		int newParentPosX = parentControl->Location.X - moveOffsetX + moveImage->Top + e->X;
		int newParentPosY = parentControl->Location.Y - moveOffsetY + moveImage->Left + e->Y;
		parentControl->Location = System::Drawing::Point(newParentPosX, newParentPosY);
	}
}
		 
private: System::Void moveBarWindow_MouseUp(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
{
	if (e->Button != System::Windows::Forms::MouseButtons::Left)
	{
		return;
	}
	mouseClick = false;
}




#pragma endregion

//=============================================START / LOGIN / LOGOUT FUNCTIONS ==============================================

#pragma region  START LOGIN LOGOUT FUNCTIONS
//Actives the board after a login.
public:	void setBlackBoardToLoggedIn(String^ _displayName)
	{
		this->loggedIn = true;
		this->displayName = _displayName;
		this->cmd_Chat->Enabled = true;
		this->cmd_Draw->Enabled = true;
		this->cmd_Map->Enabled = true;
		this->cmd_Picture->Enabled = true;
		this->cmd_UpdateBlackboard->Enabled = true;
		this->cmd_Write->Enabled = true;
		this->cmd_Settings->Enabled = true;
		this->cmd_LoginLogOut->Text = "Log Out";
	}

private: void updateBoard()
{
	this->drawPointsSteeps = blackBoardApiClass->downloadDrawPointsSteeps(blackBoardName);
	this->drawColorSteeps = blackBoardApiClass->downloadDrawColors(blackBoardName);
	this->drawSizeSteeps = blackBoardApiClass->downloadDrawSizeSteeps(blackBoardName);

	this->pictureList = blackBoardApiClass->downloadPictures(blackBoardName);
	this->writeRichTextBoxList = blackBoardApiClass->downloadWriteTextBoxes(blackBoardName);

	drawAllSteeps();
	for each (PictureBox^ pic in pictureList)
	{
		pic->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
		blackBoardPanel->Controls->Add(pic);
		addCloseImage(pic);
		addMoveBar(pic);
		addScaleImage(pic);
		for each (Control^ con in pic->Controls)
		{
			con->Hide();
		}
	}

	for each (RichTextBox^ textBox in writeRichTextBoxList)
	{
		blackBoardPanel->Controls->Add(textBox);
		addCloseImage(textBox);
		addMoveBar(textBox);
		addScaleImage(textBox);
		for each (Control^ con in textBox->Controls)
		{
			con->Hide();
		}
	}

	chat_RichTextBox->Text = blackBoardApiClass->downloadChatText(blackBoardName);
}

//Shows the login form or actives the logout button depending on the Login/logout button
private: System::Void cmd_LoginLogOut_Click(System::Object^  sender, System::EventArgs^  e)
{
	if (loggedIn == false)
	{
		this->loginForm->ShowDialog();
	}
	else
	{
		this->cmd_LoginLogOut->Text = "Log In";
		this->logOut();
	}
}


//logsout and disables the buttons
private: void logOut()
{
	this->loggedIn = false;
	this->displayName = "";
	this->cmd_Chat->Enabled = false;
	this->cmd_Draw->Enabled = false;
	this->cmd_Map->Enabled = false;
	this->cmd_Picture->Enabled = false;
	this->cmd_UpdateBlackboard->Enabled = false;
	this->cmd_Write->Enabled = false;
	this->cmd_Settings->Enabled = false;
}
#pragma endregion
};