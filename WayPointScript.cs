﻿using UnityEngine;
using System.Collections.Generic;
using System;

/// <summary>
/// This scripts finds the nearest two wayspoints and forms a train "Track" out of them. 
/// The check of neighbour1 or 2 are know tells if it is a endpoint or not
/// It uses a lot of public variables since it is a purely development Script that needs a few more iterations to get to a more refined product
/// It has been written at 2017 by Aaron Schweighöfer
/// </summary>

public class WayPointScript : MonoBehaviour
{
    [Tooltip("Is it a trainstation or not")]
    public bool isTrainStation = false;
    [Tooltip("is it a RailSwitch or not")]
    public bool isRailSwitch = false;
    public bool isEndOfLine = false;
    public WayPointScript neighbour1;
    public WayPointScript neighbour2;
    public bool neighbour1KnowsMe = false;
    public bool neighbour2KnowsMe = false;
    public float distanceToRailSwitchNeighbour1;
    public float distanceToRailSwitchNeighbour2;
    public WayPointScript nearestRailswitchNeighbour1;
    public WayPointScript nearestRailswitchNeighbour2;
    //this two list is mainly for Debug Reasons to easily see if it does go through the steeps correctly.
    public List<WayPointScript> railSwitchTracking = new List<WayPointScript>();
    public List<float> railSwitchDistancesToEachWP = new List<float>();
    public float distance = 0;

    public void SetUpWaypoint()
    {
        var railSwitch = GetComponent<RailSwitch>();
        var station = GetComponent<StationScript>();
        if (railSwitch != null)
        {
            isRailSwitch = true;
        }
        else
        {
            isRailSwitch = false;
        }
        if (station != null)
        {
            isTrainStation = true;
        }
        else
        {
            isTrainStation = false;
        }       
    }

    public void FindNearestWaypoints(List<WayPointScript> waypointListGiven)
    {
        WayPointScript thisWayPoint = transform.GetComponent<WayPointScript>();
        List<WayPointScript> waypointList = new List<WayPointScript>(waypointListGiven);
        waypointList.Remove(thisWayPoint);

        neighbour1 = FindNeighbour(waypointList);
        waypointList.Remove(neighbour1);       
        neighbour2 = FindNeighbour(waypointList);
        waypointList.Remove(neighbour2);
    }

    private WayPointScript FindNeighbour(List<WayPointScript> waypointList)
    {
        WayPointScript neighbour = waypointList[0];
        Vector3 thisWayPointPositon = transform.position;
        for (int i = 0; i < waypointList.Count; i++)
        {
            Vector3 neighbourposition = neighbour.transform.position;
            Vector3 waypointposition = waypointList[i].transform.position;
            if (Vector3.Distance(waypointposition, thisWayPointPositon) < Vector3.Distance(neighbourposition, thisWayPointPositon))
            {
                neighbour = waypointList[i];
            }
        }
        return neighbour;
    }

    private void CheckIfNeighbourKnowsMe()
    {
        WayPointScript thisWaypointscript = GetComponent<WayPointScript>();
        if (neighbour1.neighbour1 == thisWaypointscript || neighbour1.neighbour2 == thisWaypointscript)
        {
            neighbour1KnowsMe = true;
        }
        if (neighbour2.neighbour1 == thisWaypointscript || neighbour2.neighbour2 == thisWaypointscript)
        {
            neighbour2KnowsMe = true;
        }
        if(neighbour1KnowsMe == false)
        {
            Debug.Log("This One is to far away from the other Waypoints please move it closer or add another Waypoint betwen this and the rest of the track. " + gameObject.name + " / " + transform.parent.gameObject.name, gameObject);
            isEndOfLine = true;
            GameObject go = (GameObject)Instantiate(Resources.Load("ProblemSphere"), transform.position,Quaternion.identity);
            go.transform.parent = transform;
        }
        if (neighbour2KnowsMe == false && isEndOfLine == false)
        {
            Debug.Log("this waypoint apears to be End of the Line. Please make certain that it is. " + gameObject.name + " / " + transform.parent.gameObject.name,gameObject);
            isEndOfLine = true;
            GameObject go = (GameObject)Instantiate(Resources.Load("ProblemSphere"), transform.position, Quaternion.identity);
            go.transform.parent = transform;
        }
        if (isEndOfLine == true)
        {
            neighbour2 = null;
        }
    }

    public void DisplayNeighbour()
    {
        CheckIfNeighbourKnowsMe();
        Vector3 mePositon = transform.position;
        if (neighbour1KnowsMe == true)
        {
            Vector3 neighbour1Position = neighbour1.transform.position;
            Debug.DrawLine(mePositon, neighbour1Position, Color.red, 12000);
        }
        if (isEndOfLine == true)
        {
            return;
        }
        if (neighbour2KnowsMe == true)
        {
            Vector3 neighbour2Position = neighbour2.transform.position;
            Debug.DrawLine(mePositon, neighbour2Position, Color.red, 12000);
        }
    }

    //this was required for the game and was one way to accomplis the current distance to the railswitch
    public void FindDistanceToRailSwitch()
    {
        distance = 0;
        nearestRailswitchNeighbour1 = FindDistanceToNextRailSwitch(true);
        distanceToRailSwitchNeighbour1 = distance;
        if (isEndOfLine == true)
        {
            return;
        }
        distance = 0;
        nearestRailswitchNeighbour2 = FindDistanceToNextRailSwitch(false);
        distanceToRailSwitchNeighbour2 = distance;
    }

    private WayPointScript FindDistanceToNextRailSwitch(bool isneighbour1)
    {
        WayPointScript currenttarget;
        if (isneighbour1 == true)
        {
            currenttarget = neighbour1;
        }
        else
        {
            currenttarget = neighbour2;
        }
        WayPointScript oldtarget = this;
        bool targetIsRailSwitch = currenttarget.isRailSwitch;
        railSwitchTracking.Add(currenttarget);
        Vector3 currenttargetPosition;
        Vector3 oldTargetPosition;
        if (targetIsRailSwitch == true)
        {
            currenttargetPosition = currenttarget.transform.position;
            oldTargetPosition = oldtarget.transform.position;
            distance += Vector3.Distance(currenttargetPosition, oldTargetPosition);
        }
        while (targetIsRailSwitch == false)
        {
            currenttargetPosition = currenttarget.transform.position;
            oldTargetPosition = oldtarget.transform.position;
            distance += Vector3.Distance(currenttargetPosition, oldTargetPosition);
            railSwitchDistancesToEachWP.Add(distance);
            if (currenttarget.neighbour1 != oldtarget)
            {
                oldtarget = currenttarget;
                currenttarget = currenttarget.neighbour1;
                targetIsRailSwitch = currenttarget.isRailSwitch;
            }
            else if (currenttarget.isEndOfLine == false)
            {
                oldtarget = currenttarget;
                currenttarget = currenttarget.neighbour2;
                targetIsRailSwitch = currenttarget.isRailSwitch;
            }
            else
            {
                Debug.Log("End Of the Line not set or Found Error, Error, Error");
                break;
            }
            railSwitchTracking.Add(currenttarget);
            if (targetIsRailSwitch == true)
            {
                currenttargetPosition = currenttarget.transform.position;
                oldTargetPosition = oldtarget.transform.position;
                distance += Vector3.Distance(currenttargetPosition, oldTargetPosition);
            }
            if (distance > 5000)
            {
                break;
            }

        }
        railSwitchTracking.Add(null);
        return currenttarget;
    }
}
